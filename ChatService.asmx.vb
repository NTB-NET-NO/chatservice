Imports System.Web.Services

'<WebService(Namespace:="http://devserver1/ChatService/", _
'            Description:="A devserver1 chat service.")> _
<WebService(Namespace:="http://193.75.33.38/ChatService/", _
            Description:="A portal chat service.")> _
Public Class ChatService
    Inherits System.Web.Services.WebService

#Region " Web Services Designer Generated Code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Web Services Designer.
        InitializeComponent()

        'Add your own initialization code after the InitializeComponent() call

    End Sub

    'Required by the Web Services Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Web Services Designer
    'It can be modified using the Web Services Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents objVaktQueueIn As System.Messaging.MessageQueue
    Friend WithEvents objSportQueueIn As System.Messaging.MessageQueue
    Friend WithEvents objVaktQueueOut As System.Messaging.MessageQueue
    Friend WithEvents objSportQueueOut As System.Messaging.MessageQueue
    Friend WithEvents objReportQueueOut As System.Messaging.MessageQueue
    Friend WithEvents objUtenriksQueueIn As System.Messaging.MessageQueue
    Friend WithEvents objReportQueueIn As System.Messaging.MessageQueue
    Friend WithEvents objUtenriksQueueOut As System.Messaging.MessageQueue
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim configurationAppSettings As System.Configuration.AppSettingsReader = New System.Configuration.AppSettingsReader()
        Me.objVaktQueueIn = New System.Messaging.MessageQueue()
        Me.objSportQueueIn = New System.Messaging.MessageQueue()
        Me.objVaktQueueOut = New System.Messaging.MessageQueue()
        Me.objSportQueueOut = New System.Messaging.MessageQueue()
        Me.objReportQueueOut = New System.Messaging.MessageQueue()
        Me.objUtenriksQueueIn = New System.Messaging.MessageQueue()
        Me.objReportQueueIn = New System.Messaging.MessageQueue()
        Me.objUtenriksQueueOut = New System.Messaging.MessageQueue()
        '
        'objVaktQueueIn
        '
        Me.objVaktQueueIn.Formatter = New System.Messaging.ActiveXMessageFormatter()
        Me.objVaktQueueIn.MessageReadPropertyFilter.SenderId = True
        Me.objVaktQueueIn.Path = CType(configurationAppSettings.GetValue("objVaktQueue.Path", GetType(System.String)), String)
        '
        'objSportQueueIn
        '
        Me.objSportQueueIn.Formatter = New System.Messaging.ActiveXMessageFormatter()
        Me.objSportQueueIn.MessageReadPropertyFilter.SenderId = True
        Me.objSportQueueIn.Path = CType(configurationAppSettings.GetValue("objSportQueue.Path", GetType(System.String)), String)
        '
        'objVaktQueueOut
        '
        Me.objVaktQueueOut.Formatter = New System.Messaging.ActiveXMessageFormatter()
        Me.objVaktQueueOut.Path = CType(configurationAppSettings.GetValue("objVaktQueueOut.Path", GetType(System.String)), String)
        '
        'objSportQueueOut
        '
        Me.objSportQueueOut.Formatter = New System.Messaging.ActiveXMessageFormatter()
        Me.objSportQueueOut.Path = CType(configurationAppSettings.GetValue("objSportQueueOut.Path", GetType(System.String)), String)
        '
        'objReportQueueOut
        '
        Me.objReportQueueOut.Formatter = New System.Messaging.ActiveXMessageFormatter()
        Me.objReportQueueOut.Path = CType(configurationAppSettings.GetValue("objReportQueueOut.Path", GetType(System.String)), String)
        '
        'objUtenriksQueueIn
        '
        Me.objUtenriksQueueIn.Formatter = New System.Messaging.ActiveXMessageFormatter()
        Me.objUtenriksQueueIn.MessageReadPropertyFilter.SenderId = True
        Me.objUtenriksQueueIn.Path = CType(configurationAppSettings.GetValue("objUtenriksQueue.Path", GetType(System.String)), String)
        '
        'objReportQueueIn
        '
        Me.objReportQueueIn.Formatter = New System.Messaging.ActiveXMessageFormatter()
        Me.objReportQueueIn.MessageReadPropertyFilter.SenderId = True
        Me.objReportQueueIn.Path = CType(configurationAppSettings.GetValue("objReportQueue.Path", GetType(System.String)), String)
        '
        'objUtenriksQueueOut
        '
        Me.objUtenriksQueueOut.Formatter = New System.Messaging.ActiveXMessageFormatter()
        Me.objUtenriksQueueOut.Path = CType(configurationAppSettings.GetValue("objUtenriksQueueOut.Path", GetType(System.String)), String)

    End Sub

    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        'CODEGEN: This procedure is required by the Web Services Designer
        'Do not modify it using the code editor.
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

#End Region

    ' WEB SERVICE EXAMPLE
    ' The HelloWorld() example service returns the string Hello World.
    ' To build, uncomment the following lines then save and build the project.
    ' To test this web service, ensure that the .asmx file is the start page
    ' and press F5.
    '
    <WebMethod(Description:="HelloWorld Test")> _
    Public Function HelloWorld() As String
        HelloWorld = "Hello World Roar!"
    End Function

    <WebMethod(Description:="Send To Sports Queue")> _
    Public Sub SendToSportsQueue(ByRef strSessionID As String, ByRef strMsg As String)
        Dim ts As New TimeSpan(10000)
        Dim oMsg As New System.Messaging.Message()
        Dim objFormatter As New System.Messaging.ActiveXMessageFormatter()
        SyncLock (objSportQueueOut)
            oMsg.Formatter = objFormatter
            oMsg.Label = CStr(strSessionID)
            oMsg.Body = CStr(strMsg)

            Try
                objSportQueueOut.Send(oMsg)
            Catch
            End Try
        End SyncLock
        oMsg = Nothing
        objFormatter = Nothing
    End Sub

    <WebMethod(Description:="Send To Report Queue")> _
    Public Sub SendToReportQueue(ByRef strSessionID As String, ByRef strMsg As String)
        Dim ts As New TimeSpan(10000)
        Dim oMsg As New System.Messaging.Message()
        Dim objFormatter As New System.Messaging.ActiveXMessageFormatter()
        SyncLock (objReportQueueOut)
            oMsg.Formatter = objFormatter
            oMsg.Label = CStr(strSessionID)
            oMsg.Body = CStr(strMsg)

            Try
                objReportQueueOut.Send(oMsg)
            Catch
            End Try
        End SyncLock
        oMsg = Nothing
        objFormatter = Nothing

    End Sub

    <WebMethod(Description:="Send To Vaktsjef Queue")> _
    Public Sub SendToVaktQueue(ByRef strSessionID As String, ByRef strMsg As String)
        Dim ts As New TimeSpan(10000)
        Dim oMsg As New System.Messaging.Message()
        Dim objFormatter As New System.Messaging.ActiveXMessageFormatter()
        SyncLock (objVaktQueueOut)
            oMsg.Formatter = objFormatter
            oMsg.Label = CStr(strSessionID)
            oMsg.Body = CStr(strMsg)

            Try
                objVaktQueueOut.Send(oMsg)
            Catch
            End Try
        End SyncLock
        oMsg = Nothing
        objFormatter = Nothing
    End Sub

    <WebMethod(Description:="Send To Utenriks Queue")> _
    Public Sub SendToUtenriksQueue(ByRef strSessionID As String, ByRef strMsg As String)
        Dim ts As New TimeSpan(10000)
        Dim oMsg As New System.Messaging.Message()
        Dim objFormatter As New System.Messaging.ActiveXMessageFormatter()
        SyncLock (objUtenriksQueueOut)
            oMsg.Formatter = objFormatter
            oMsg.Label = CStr(strSessionID)
            oMsg.Body = CStr(strMsg)

            Try
                objUtenriksQueueOut.Send(oMsg)
            Catch
            End Try
        End SyncLock
        oMsg = Nothing
        objFormatter = Nothing
    End Sub

    <WebMethod(Description:="Read Sports Queue")> _
    Public Sub ReadSportsQueue(ByRef strSessionID As String, ByRef strMsg As String)
        Dim ts As New TimeSpan(10000)
        Dim oMsg As New System.Messaging.Message()
        Dim iFormatter As New System.Messaging.ActiveXMessageFormatter()
        Try
            oMsg = objSportQueueIn.Receive(ts)
            oMsg.Formatter = iFormatter
            strSessionID = CStr(oMsg.Label)
            strMsg = CStr(oMsg.Body)
        Catch
            'strSessionID = Err.Number
            'strMsg = Err.Description & ": " & objSportQueueIn.Path
        End Try
    End Sub

    <WebMethod(Description:="Read Report Queue")> _
    Public Sub ReadReportQueue(ByRef strSessionID As String, ByRef strMsg As String)
        Dim ts As New TimeSpan(10000)
        Dim oMsg As New System.Messaging.Message()
        Dim iFormatter As New System.Messaging.ActiveXMessageFormatter()
        Try
            oMsg = objReportQueueIn.Receive(ts)
            oMsg.Formatter = iFormatter
            strSessionID = CStr(oMsg.Label)
            strMsg = CStr(oMsg.Body)
        Catch
        End Try
    End Sub

    <WebMethod(Description:="Read Vaktsjef Queue")> _
    Public Sub ReadVaktQueue(ByRef strSessionID As String, ByRef strMsg As String)
        Dim ts As New TimeSpan(10000)
        Dim oMsg As New System.Messaging.Message()
        Dim iFormatter As New System.Messaging.ActiveXMessageFormatter()
        Try
            oMsg = objVaktQueueIn.Receive(ts)
            oMsg.Formatter = iFormatter
            strSessionID = CStr(oMsg.Label)
            strMsg = CStr(oMsg.Body)
        Catch
        End Try
    End Sub

    <WebMethod(Description:="Read Utenriks Queue")> _
    Public Sub ReadUtenriksQueue(ByRef strSessionID As String, ByRef strMsg As String)
        Dim ts As New TimeSpan(10000)
        Dim oMsg As New System.Messaging.Message()
        Dim iFormatter As New System.Messaging.ActiveXMessageFormatter()
        Try
            oMsg = objUtenriksQueueIn.Receive(ts)
            oMsg.Formatter = iFormatter
            strSessionID = CStr(oMsg.Label)
            strMsg = CStr(oMsg.Body)
        Catch
        End Try
    End Sub

End Class
